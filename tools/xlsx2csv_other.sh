#!/bin/bash
#######################
### convert xls to txt
###
### based on ssconvert
### -O = OPTIONS FOR THE CONFIGURABLE TEXT (*.txt) EXPORTER. !!!!!! The output file name extension must be .txt and not .csv in order to succeed
### charset : The character encoding of the output. Defaults to UTF-8
### sheet : Name of the workbook sheet to operate on.  You can specify several sheets by repeating this option. If this option is not given the active sheet (i. e. the sheet that was active when the file was saved) is used.  This is ignored if the object option is given.
### separator : The string used to separate fields. Defaults to space.
### quoting-mode : When does data need to be quoted?  "never", "auto" (puts quotes where needed), or "always". Defaults to "never".
### format : How cells should be formatted.  Acceptable values: "automatic" (apply automatic formatting; default), "raw" (output data raw, unformatted), or "preserve" (preserve  the  formatting from the source document).


path="."
inputDir=${path}


for f in ${inputDir}/*_ISO88591*.xls*; do 
        
    echo '######################'
    echo "Processing file : $f "
 
     filename=$(basename $f)
     # Exemple of filename : doi_benin_ISO88591.xlsx
     # to retrieve the filename extension
     extension=${filename##*.}
     # to suppress the extension and a part of the name: result = template_dataset_AL.Met_Od
     filename_noext=${filename%_ISO88591*}

     outputFileName1=${inputDir}/${filename_noext}.txt
     outputFileName2=${inputDir}/${filename_noext}.csv

     ssconvert -O 'separator=; format=preserve  quoting-mode=auto' $f $outputFileName1
     mv $outputFileName1 $outputFileName2
done
