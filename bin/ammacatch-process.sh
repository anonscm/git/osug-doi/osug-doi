#!/bin/bash

PROJECT=AMMA-CATCH

DO_GET=0

CONF=../conf/$PROJECT
INPUT=$CONF/inputs/
TMP=../tmp/$PROJECT

RES=../resources
XSL=$RES/xsl

rm -rf $TMP
mkdir -p $TMP


# 0. GetRecords from bd.amma-catch.org:
if [ $DO_GET -eq 1 ]
then
    wget -O $TMP/getRecords.xml "http://bd.amma-catch.org/amma-catchWS2/WS/csw/default?service=CSW&request=GetRecords&version=2.0.2&typenames=csw:Record&resulttype=results&maxrecords=100&elementsetname=summary&outputschema=http://www.isotc211.org/2005/gmd"

    xsltproc -o $INPUT/getRecords.csv --stringparam DOI_SUFFIX_START "$PROJECT" $XSL/csw2txt.xsl $TMP/getRecords.xml

    if [ $? -ne 0 ]; then
         echo "xsltproc failed"
         exit 1
    fi
fi

bash doimgr-process.sh $PROJECT

echo "done."

