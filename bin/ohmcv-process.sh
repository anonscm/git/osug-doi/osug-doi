#!/bin/bash

PROJECT=OHMCV

DO_GET=0
DO_WGET=0

CONF=../conf/$PROJECT
INPUT=$CONF/inputs/
TMP=../tmp/$PROJECT

RES=../resources
XSL=$RES/xsl

#rm -rf $TMP
mkdir -p $TMP


# 0. GetRecords from sedoo:
if [ $DO_GET -eq 1 ]
then
    # Prepare data access url mapping:
    echo "# DOI_suffix;data_access_url" > $CONF/doi_url_data_access.csv

    for row in $(cat $INPUT/doi_id.txt)
    do
        doi=${row%;*}
        id=${row#*;}

        echo "doi: $doi"
#        echo "id:  $id"

        url="http://mistrals.sedoo.fr/xml/bdToIso.php?datsId=$id"

        if [ ${url:0:4} = "http" ]; then
#            echo $url

            if [ $DO_WGET -eq 1 ]
            then
                wget -O $TMP/${doi}.xml $url
                cp $TMP/${doi}.xml $INPUT/mistrals/
            fi

            # NOTE: fix manually bad <xml> header

            xsltproc -o $INPUT/${doi}.csv --stringparam DOI_SUFFIX_START "$PROJECT" --stringparam USE_END_DATE "true" $XSL/csw2txt_ohmcv.xsl $INPUT/mistrals/${doi}.xml

            # replace Hymex ID by DOI
            EXPR="s/identifier:DOI;.*/identifier:DOI;10.5072\/${doi}/"

            sed -i "$EXPR" $INPUT/${doi}.csv

            # add url data access:
            url="http://www.ohmcv.fr/P000_download.php?fic=$id"
            echo "${doi};$url" >> $CONF/doi_url_data_access.csv

            if [ $? -ne 0 ]; then
                 echo "xsltproc failed"
                 exit 1
            fi
        fi
    done

    exit 0
fi

bash doimgr-process.sh $PROJECT

echo "done."

