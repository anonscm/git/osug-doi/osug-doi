#!/bin/bash

# initialize environment:
source env.sh


if [ "$#" -eq  "0" ]
  then
    PROJECT=""
else
    PROJECT="--project=$1"
fi

# mode=staging|public|all
MODE="--mode=all"


# Generate only landing pages from database:
$APP_CMD --action=generate $PROJECT $MODE

if [ $? -ne 0 ]; then
  echo "java process failed"
  exit 1
fi

