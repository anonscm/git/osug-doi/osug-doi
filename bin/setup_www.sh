#!/bin/bash

# To setup www, just run:
#   bash setup_www.sh
# To setup the tests, run:
#   bash setup_www.sh ../target/test-output/www/ 

DO_GET=0

# bootstrap
BS_VER=3.3.7
BS=bootstrap-${BS_VER}-dist
BS_URL="https://github.com/twbs/bootstrap/releases/download/v${BS_VER}/${BS}.zip"

RES=../resources
STATIC=$RES/static
XSD=$RES/xsd/

if [ "$#" -eq  "0" ]
  then
    WEB=../www
else
    WEB=$1
fi

PUBLIC=$WEB/public
STAGING=$WEB/staging
WEB_XSD=$WEB/xsd/

# create folders
mkdir -p $PUBLIC
mkdir -p $STAGING
mkdir -p $WEB_XSD

# publish xsd
cp -r $XSD/* $WEB_XSD


if [ $DO_GET -eq 1 ]
then
    # prepare bootstrap
    wget $BS_URL
    if [ $? -ne 0 ]; then
      echo "unable to download bootstrap"
      exit 1
    fi
    rm -rf $WEB/bootstrap*
    unzip $BS.zip -d $WEB
    ln -s $BS $WEB/bootstrap
    rm $BS.zip
fi

# prepare static content:
cp -r $STATIC/* $WEB

echo "setup: done."

