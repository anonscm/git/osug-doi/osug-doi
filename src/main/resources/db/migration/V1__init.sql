
/* PROJECT */
create sequence project_id_seq START 1 INCREMENT 10;

create table project (
    id int8 not null,
    name varchar(32) not null,
    description varchar(512),
    create_date timestamp,
    update_date timestamp,
    primary key (id)
);
alter table project add constraint uk_project_name unique (name);

/* DOI */
create sequence doi_id_seq  START 1 INCREMENT 10;

create table doi (
    id int8 not null,
    project_id int8 not null,
    identifier varchar(255) not null,
    status varchar(16),
    description varchar(512),
    primary key (id)
);
alter table doi add constraint uk_doi_identifier unique (identifier);
alter table doi add constraint fk_doi_project_id foreign key (project_id) references project;

/* DOI COMMON (seq) */
create sequence doi_common_id_seq  START 1 INCREMENT 10;

/* DOI STAGING */
create table doi_staging (
    /* DOI Common fields */
    id int8 not null,
    doi_id int8 not null,
    create_date timestamp,
    update_date timestamp,
    metadata_md5 varchar(32) not null,
    data_access_url varchar(1024),
    landing_ext_url varchar(1024),
    landing_loc_url varchar(512),
    dc_metadata_md5 varchar(32),
    dc_url varchar(512),
    /* specific fields */
    valid boolean,
    log varchar(4096),
    url_valid boolean,
    url_log varchar(4096),
    primary key (id)
);
alter table doi_staging add constraint fk_doi_staging_id foreign key (doi_id) references doi;

/* DOI PUBLIC */
create table doi_public (
    /* DOI Common fields */
    id int8 not null,
    doi_id int8 not null,
    create_date timestamp,
    update_date timestamp,
    metadata_md5 varchar(32) not null,
    data_access_url varchar(1024),
    landing_ext_url varchar(1024),
    landing_loc_url varchar(512),
    dc_metadata_md5 varchar(32),
    dc_url varchar(512),
    /* specific fields */
    active_ext_url boolean,
    primary key (id)
);
alter table doi_public add constraint fk_doi_public_id foreign key (doi_id) references doi;

/* EOF */