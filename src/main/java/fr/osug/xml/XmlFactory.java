/*******************************************************************************
 * OSUG-DOI project ( http://doi.osug.fr ) - Copyright (C) CNRS.
 ******************************************************************************/
package fr.osug.xml;

import fr.osug.util.FileUtils;
import fr.osug.util.StringBuilderWriter;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Source;
import javax.xml.transform.Templates;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 * Utility class for XML parsing & transforming (XSLT)  <br><b>Supports XML document & XSLT caches</b>
 */
@Component
public final class XmlFactory {

    /** class logger */
    private static Logger log = LoggerFactory.getLogger(XmlFactory.class.getName());

    /** encoding used for XML and XSL documents */
    public static final String ENCODING = "UTF-8";
    /** default buffer size (4 ko) for XSLT result document */
    public static final int DEFAULT_BUFFER_SIZE = 4096;

    // members:
    /** inner DOM factory */
    private DocumentBuilderFactory documentFactory = null;
    /** inner XSLT factory */
    private TransformerFactory transformerFactory = null;
    /** inner Schema factory */
    private SchemaFactory schemaFactory = null;
    /** cache for Dom instances */
    private final Map<String, Document> cacheDOM = new HashMap<String, Document>(32);
    /** cache for Xsl templates */
    private final Map<String, Templates> cacheXSL = new HashMap<String, Templates>(32);

    /**
     * Creates a new XmlFactory object
     */
    public XmlFactory() {
        /* no-op */
    }

    /**
     * Returns a DocumentBuilderFactory (JAXP)
     *
     * @return DocumentBuilderFactory (JAXP)
     */
    public final DocumentBuilderFactory getFactory() {
        if (documentFactory == null) {
            documentFactory = DocumentBuilderFactory.newInstance();
        }

        return documentFactory;
    }

    /**
     * Returns a TransformerFactory (JAXP)
     *
     * @return TransformerFactory (JAXP)
     */
    private final TransformerFactory getTransformerFactory() {
        if (transformerFactory == null) {
            try {
                transformerFactory = TransformerFactory.newInstance();

                log.debug("transformerFactory: {}", transformerFactory);

            } catch (final TransformerFactoryConfigurationError tfce) {
                throw new IllegalStateException("XmlFactory.getTransformerFactory : failure on TransformerFactory initialisation : ", tfce);
            }
        }

        return transformerFactory;
    }

    /**
     * Returns a SchemaFactory (JAXP)
     *
     * @return SchemaFactory (JAXP)
     */
    private final SchemaFactory getSchemaFactory() {
        if (schemaFactory == null) {

            // 1. Lookup a factory for the W3C XML Schema language
            schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
        }

        return schemaFactory;
    }

    /**
     * Retrieve a schema instance from the given URL
     *
     * @param schemaURL schema URI
     *
     * @return schema instance
     *
     * @throws IllegalStateException if the schema can be retrieved or parsed
     */
    public Schema getSchema(final String schemaURL) {
        Schema s = null;
        try {
            final URL url = (schemaURL.startsWith("file:")) ? new URL(schemaURL) : FileUtils.getResource(schemaURL);

            if (log.isInfoEnabled()) {
                log.info("XmlFactory.getSchema : retrieve schema and compile it : {}", schemaURL);
            }

            // 2. Compile the schema.
            SchemaFactory sf = getSchemaFactory();
            s = sf.newSchema(url);

            if (log.isInfoEnabled()) {
                log.info("XmlFactory.getSchema : schema ready : {}", s);
            }

        } catch (final SAXException se) {
            throw new IllegalStateException("XmlFactory.getSchema : unable to create a Schema for : " + schemaURL, se);
        } catch (MalformedURLException mue) {
            throw new IllegalStateException("XmlFactory.getSchema : unable to create a Schema for : " + schemaURL, mue);
        }
        return s;
    }

    /**
     * Returns an Identity transformer (identity xslt)
     *
     * @return identity transformer
     */
    private final Transformer newTransformer() {
        try {
            return getOutTransformer(getTransformerFactory().newTransformer());
        } catch (final TransformerConfigurationException tce) {
            throw new IllegalStateException("XmlFactory.newTransformer : failure on creating new Transformer : ", tce);
        }
    }

    /**
     * Returns a transformer for the given xslt source
     *
     * @param source stream source for xslt script
     *
     * @return transformer for the given xslt source
     */
    private final Transformer newTransformer(final StreamSource source) {
        try {
            return getOutTransformer(getTransformerFactory().newTransformer(source));
        } catch (final TransformerConfigurationException tce) {
            throw new IllegalStateException("XmlFactory.newTransformer : failure on creating new Transformer for source : " + source, tce);
        }
    }

    /**
     * Returns a transformer for the given xslt template (precompiled xslt script)
     *
     * @param tmp xslt template (precompiled xslt script)
     *
     * @return transformer for the given xslt template
     */
    private final Transformer newTransformer(final Templates tmp) {
        try {
            return getOutTransformer(tmp.newTransformer());
        } catch (final TransformerConfigurationException tce) {
            throw new IllegalStateException("XmlFactory.newTransformer : failure on creating new Transformer for template : " + tmp, tce);
        }
    }

    /**
     * Returns a new xslt template (precompiled xslt script) for the given xslt source
     *
     * @param source stream source for xslt script
     *
     * @return new xslt template
     */
    private final Templates newTemplate(final StreamSource source) {
        try {
            return getTransformerFactory().newTemplates(source);
        } catch (final TransformerConfigurationException tce) {
            throw new IllegalStateException("XmlFactory.newTransformer : failure on creating new template : " + source, tce);
        }
    }

    /**
     * Sets the encoding and indetation parameters for the given transformer
     *
     * @param tf transformer
     *
     * @return tf transformer
     */
    private static final Transformer getOutTransformer(final Transformer tf) {
        tf.setOutputProperty(OutputKeys.ENCODING, ENCODING);
        tf.setOutputProperty(OutputKeys.INDENT, "yes");

        return tf;
    }

    /**
     * Parses a local xml file with JAXP
     *
     * @param f local file
     *
     * @return Document (DOM)
     */
    public final Document parse(final File f) {
        String uri = "file:" + f.getAbsolutePath();

        if (File.separatorChar == '\\') {
            uri = uri.replace('\\', '/');
        }

        return parse(new InputSource(uri));
    }

    /**
     * Parses an xml stream with JAXP
     *
     * @param is input stream
     *
     * @return Document (DOM)
     */
    public final Document parse(final InputStream is) {
        final InputSource in = new InputSource(is);

        return parse(in);
    }

    /**
     * Parses an xml stream with JAXP
     *
     * @param is input stream
     * @param systemId absolute file or URL reference used to resolve other xml document references
     *
     * @return Document (DOM)
     */
    public final Document parse(final InputStream is, final String systemId) {
        final InputSource in = new InputSource(is);

        in.setSystemId(systemId);

        return parse(in);
    }

    /**
     * Parses an xml stream with JAXP
     *
     * @param input xml input source
     *
     * @return Document (DOM)
     */
    public final Document parse(final InputSource input) {
        if (log.isDebugEnabled()) {
            log.debug("XmlFactory.parse : begin");
        }

        Document document = null;

        try {
            input.setEncoding(ENCODING);
            document = getFactory().newDocumentBuilder().parse(input);
        } catch (final SAXException se) {
            throw new IllegalStateException("XmlFactory.parse : error", se);
        } catch (final IOException ioe) {
            throw new IllegalStateException("XmlFactory.parse : error", ioe);
        } catch (final ParserConfigurationException pce) {
            throw new IllegalStateException("XmlFactory.parse : error", pce);
        }

        if (log.isInfoEnabled()) {
            log.info("XmlFactory.parse : exit : {}", document);
        }

        return document;
    }

    /**
     * Process xslt on xml document XSL code can use parameter 'lastModified'
     *
     * @param source XML source to transform
     * @param xslFilePath XSL file to use (XSLT)
     * @param doCacheXsl true indicates that XSLT can be keep in permanent cache for reuse (avoid a lot of wasted time
     *        (compiling xslt) for many transformations)
     *
     * @return result document
     */
    public String transform(final Source source, final String xslFilePath, final boolean doCacheXsl) {
        return transform(source, xslFilePath, null, doCacheXsl);
    }

    /**
     * Process xslt on xml document XSL code can use parameter 'lastModified'
     *
     * @param source XML source to transform
     * @param xslFilePath XSL file to use (XSLT)
     * @param parameters simple parameters
     * @param doCacheXsl true indicates that XSLT can be keep in permanent cache for reuse (avoid a lot of wasted time
     *        (compiling xslt) for many transformations)
     *
     * @return result document
     */
    public String transform(final Source source, final String xslFilePath,
                            final java.util.Map<String, Object> parameters,
                            final boolean doCacheXsl) {
        final StringBuilderWriter out = new StringBuilderWriter(DEFAULT_BUFFER_SIZE);

        transform(source, xslFilePath, parameters, doCacheXsl, new StreamResult(out));

        return out.toString();
    }

    /**
     * Process xslt on xml document
     *
     * @param source XML source to transform
     * @param xslFilePath XSL file to use (XSLT)
     * @param doCacheXsl true indicates that XSLT can be keep in permanent cache for reuse (avoid a lot of wasted time
     *        (compiling xslt) for many transformations)
     * @param out result holder
     */
    public void transform(final Source source, final String xslFilePath, final boolean doCacheXsl,
                          final StreamResult out) {
        transform(source, xslFilePath, null, doCacheXsl, out);
    }

    /**
     * Process xslt on xml document XSL code can use parameter 'lastModified'
     *
     * @param source XML source to transform
     * @param xslFilePath XSL file to use (XSLT)
     * @param parameters simple parameters
     * @param doCacheXsl true indicates that XSLT can be keep in permanent cache for reuse (avoid a lot of wasted time
     *        (compiling xslt) for many transformations)
     * @param out result holder
     */
    public void transform(final Source source, final String xslFilePath,
                          final java.util.Map<String, Object> parameters,
                          final boolean doCacheXsl, final StreamResult out) {
        if (log.isDebugEnabled()) {
            log.debug("XmlFactory.transform : enter : xslFilePath : {}", xslFilePath);
        }

        if ((source != null) && (xslFilePath != null)) {
            final long startTime = System.nanoTime();

            final Transformer tf;

            if (doCacheXsl) {
                tf = loadXsl(xslFilePath);
            } else {
                final File file = new File(xslFilePath);

                tf = newTransformer(new StreamSource(file));
            }

            if (tf != null) {
                if (parameters != null) {
                    if (log.isDebugEnabled()) {
                        log.debug("XmlFactory.transform : XSL parameters : {}", parameters);
                    }
                    for (final Map.Entry<String, Object> entry : parameters.entrySet()) {
                        tf.setParameter(entry.getKey(), entry.getValue());
                    }
                }

                if (log.isDebugEnabled()) {
                    log.debug("XmlFactory.transform : XML Source : {}", source);
                }

                asString(tf, source, out);
            }

            if (log.isDebugEnabled()) {
                log.debug("transform: duration = {} ms.", 1e-6d * (System.nanoTime() - startTime));
            }
        }

        if (log.isDebugEnabled()) {
            log.debug("XmlFactory.transform : exit : {}", out);
        }
    }

    /**
     * Loads xml template with cache
     *
     * @param absoluteFilePath absolute file path for xml document
     *
     * @return XML Document or null if file does not exist or not (xml) valid
     */
    public final Document loadTemplate(final String absoluteFilePath) {
        if ((absoluteFilePath == null) || (absoluteFilePath.length() == 0)) {
            log.error("XmlFactory.loadTemplate : unable to load template : empty file name !");

            return null;
        }

        Document doc = cacheDOM.get(absoluteFilePath);

        if (doc == null) {
            final File file = new File(absoluteFilePath);

            if (!file.exists()) {
                log.error("XmlFactory.loadTemplate : unable to load template : no file found for : {}", absoluteFilePath);

                return null;
            }

            if (log.isDebugEnabled()) {
                log.debug("XmlFactory.loadTemplate : file : {}", file);
            }

            doc = parse(file);

            if (doc != null) {
                cacheDOM.put(absoluteFilePath, doc);

                if (log.isDebugEnabled()) {
                    log.debug(
                            "XmlFactory.loadTemplate : template : " + Integer.toHexString(doc.hashCode()) + " : \n" + asString(doc));
                }
            }
        }

        if (doc != null) {
            if (log.isDebugEnabled()) {
                log.debug("XmlFactory.loadTemplate : template in use : {}", Integer.toHexString(doc.hashCode()));
            }

            doc = (Document) doc.cloneNode(true);
        }

        if (log.isDebugEnabled()) {
            log.debug("XmlFactory.loadTemplate : xml document :\n{}", asString(doc));
        }

        return doc;
    }

    /**
     * Loads xsl template with cache
     *
     * @param absoluteFilePath absolute file path for xsl document
     *
     * @return transformer or null if file does not exist or xslt not valid
     */
    public final Transformer loadXsl(final String absoluteFilePath) {
        if ((absoluteFilePath == null) || (absoluteFilePath.length() == 0)) {
            log.error("XmlFactory.loadXsl : unable to load template : empty file name !");

            return null;
        }

        Transformer tf = null;
        Templates tmp = cacheXSL.get(absoluteFilePath);

        if (tmp == null) {
            final File file = new File(absoluteFilePath);

            if (!file.exists()) {
                log.error("XmlFactory.loadXsl : unable to load xslt : no file found for : {}", absoluteFilePath);

                return null;
            }

            if (log.isDebugEnabled()) {
                log.debug("XmlFactory.loadXsl : file : {}", file);
            }

            tmp = newTemplate(new StreamSource(file));
            cacheXSL.put(absoluteFilePath, tmp);

            if (log.isDebugEnabled()) {
                log.debug("XmlFactory.loadXsl : template : {}", Integer.toHexString(tmp.hashCode()));
            }
        }

        if (tmp != null) {
            if (log.isDebugEnabled()) {
                log.debug("XmlFactory.loadXsl : template in cache : {}", Integer.toHexString(tmp.hashCode()));
            }

            tf = newTransformer(tmp);
        }

        if (log.isDebugEnabled()) {
            log.debug("XmlFactory.loadXsl : xslt : {}", tf);
        }

        return tf;
    }

    /**
     * Converts node hierarchy with identity transformer
     *
     * @param node xml node (root)
     *
     * @return String produced by transformer
     */
    public String asString(final Node node) {
        if (node == null) {
            return "null";
        }

        return asString(new DOMSource(node), DEFAULT_BUFFER_SIZE);
    }

    /**
     * Converts node hierarchy with identity transformer
     *
     * @param node xml node (root)
     * @param bufferSize buffer size
     *
     * @return String produced by transformer
     */
    public String asString(final Node node, final int bufferSize) {
        if (node == null) {
            return "null";
        }

        return asString(new DOMSource(node), bufferSize);
    }

    /**
     * Converts source nodes with identity transformer
     *
     * @param source xml nodes
     * @param bufferSize buffer size
     *
     * @return String produced by transformer
     */
    public String asString(final Source source, final int bufferSize) {
        return asString(newTransformer(), source, bufferSize);
    }

    /**
     * Converts source nodes to a string with given transformer
     *
     * @param transformer XSL transformer to use
     * @param node xml node (root)
     *
     * @return String produced by transformer
     */
    public static String asString(final Transformer transformer, final Node node) {
        if (node == null) {
            return "null";
        }

        return asString(transformer, new DOMSource(node), DEFAULT_BUFFER_SIZE);
    }

    /**
     * Converts source nodes to a string with given transformer
     *
     * @param transformer XSL transformer to use
     * @param source xml nodes
     * @param bufferSize buffer size
     *
     * @return String produced by transformer
     */
    public static String asString(final Transformer transformer, final Source source, final int bufferSize) {
        final StringBuilderWriter out = new StringBuilderWriter(bufferSize);

        asString(transformer, source, new StreamResult(out));

        return out.toString();
    }

    /**
     * Converts source nodes into the String buffer with given transformer
     *
     * @param transformer XSL transformer to use
     * @param source xml nodes
     * @param out buffer (should be cleared before method invocation)
     */
    public static void asString(final Transformer transformer, final Source source, final StreamResult out) {
        try {
            transformer.transform(source, out);
        } catch (final TransformerException te) {
            throw new IllegalStateException("XmlFactory.asString : transformer failure :", te);
        }
    }
}
