/*******************************************************************************
 * OSUG-DOI project ( http://doi.osug.fr ) - Copyright (C) CNRS.
 ******************************************************************************/
package fr.osug.xml.validator;

import java.io.IOException;

import javax.xml.transform.Source;
import javax.xml.validation.Schema;
import javax.xml.validation.Validator;

import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

/**
 * XML Document Validator against an xml schema
 */
public final class XmlValidator {

    /** XML Schema instance */
    private final Schema schema;

    /**
     * Constructor for a given schema URL
     * @param schema Schema instance
     */
    XmlValidator(Schema schema) {
        this.schema = schema;
    }

    /**
     * Validate the given XML stream according to the schema
     *
     * @param source xml source
     *
     * @return ValidationResult instance
     */
    public ValidationResult validate(final Source source) {
        return validate(source, new ValidationResult());
    }

    /**
     * Validate the given XML stream according to the schema
     *
     * @param source xml source
     * @param result ValidationResult instance
     *
     * @return ValidationResult instance
     */
    public ValidationResult validate(final Source source, final ValidationResult result) {
        if (result == null) {
            throw new IllegalArgumentException("ValidationResult is null !");
        }
        // 3. Get a validator from the schema.
        final Validator validator = this.schema.newValidator();

        validator.setErrorHandler(new CustomErrorHandler(result));

        try {
            // 5. Check the document
            validator.validate(source);
        } catch (final SAXException se) {
            // intercepted by CustomErrorHandler
            result.getMessages().add(new ErrorMessage(ErrorMessage.SEVERITY.FATAL, -1, -1, se.getMessage()));
        } catch (final IOException ioe) {
            // intercepted by CustomErrorHandler
            result.getMessages().add(new ErrorMessage(ErrorMessage.SEVERITY.FATAL, -1, -1, ioe.getMessage()));
        }

        if (result.getMessages().isEmpty()) {
            result.setValid(true);
        }

        return result;
    }

    /**
     * SAX ErrorHandler implementation to add validation exception to the given ValidationResult instance
     * @see org.xml.sax.ErrorHandler
     */
    private final class CustomErrorHandler implements ErrorHandler {

        /** validation result */
        private ValidationResult result;

        /**
         * Public constructor with the given validation result
         * @param pResult validation result to use
         */
        CustomErrorHandler(final ValidationResult pResult) {
            this.result = pResult;
        }

        /**
         * Wrap the SAX exception in an ErrorMessage instance added to the validation result
         * @param se SAX parse exception 
         * @see org.xml.sax.ErrorHandler#warning(SAXParseException)
         */
        @Override
        public void warning(final SAXParseException se) {
            result.getMessages().add(
                    new ErrorMessage(ErrorMessage.SEVERITY.WARNING, se.getLineNumber(), se.getColumnNumber(), se.getMessage()));
        }

        /**
         * Wrap the SAX exception in an ErrorMessage instance added to the validation result
         * @param se SAX parse exception 
         * @see org.xml.sax.ErrorHandler#error(SAXParseException)
         */
        @Override
        public void error(final SAXParseException se) {
            result.getMessages().add(
                    new ErrorMessage(ErrorMessage.SEVERITY.ERROR, se.getLineNumber(), se.getColumnNumber(), se.getMessage()));
        }

        /**
         * Wrap the SAX exception in an ErrorMessage instance added to the validation result
         * @param se SAX parse exception 
         * @see org.xml.sax.ErrorHandler#fatalError(SAXParseException)
         */
        @Override
        public void fatalError(final SAXParseException se) {
            result.getMessages().add(
                    new ErrorMessage(ErrorMessage.SEVERITY.FATAL, se.getLineNumber(), se.getColumnNumber(), se.getMessage()));
        }
    }
}
