/*******************************************************************************
 * OSUG-DOI project ( http://doi.osug.fr ) - Copyright (C) CNRS.
 ******************************************************************************/
package fr.osug.xml.validator;

import fr.osug.xml.XmlFactory;
import java.util.concurrent.ConcurrentHashMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class XmlValidatorFactory {

    /** class logger */
    private static Logger log = LoggerFactory.getLogger(XmlValidator.class.getName());

    // members
    private XmlFactory xmlFactory;

    /** all factories */
    private final ConcurrentHashMap<String, XmlValidator> managedInstances = new ConcurrentHashMap<String, XmlValidator>(4);

    @Autowired
    public XmlValidatorFactory(final XmlFactory xmlFactory) {
        this.xmlFactory = xmlFactory;
    }
    
    /**
     * Factory singleton per schema URL pattern
     *
     * @param schemaURL URL for the schema to validate against
     *
     * @return XMLValidator initialized
     */
    public final XmlValidator getInstance(final String schemaURL) {
        XmlValidator v = managedInstances.get(schemaURL);

        if (v == null) {
            if (log.isInfoEnabled()) {
                log.info("XMLValidator.getInstance : creating new instance for : " + schemaURL);
            }

            v = new XmlValidator(xmlFactory.getSchema(schemaURL));

            managedInstances.putIfAbsent(schemaURL, v);
            // to be sure to return the singleton :
            v = managedInstances.get(schemaURL);
        }
        return v;
    }

}
