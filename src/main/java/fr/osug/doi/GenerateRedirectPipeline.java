/*******************************************************************************
 * OSUG-DOI project ( http://doi.osug.fr ) - Copyright (C) CNRS.
 ******************************************************************************/
package fr.osug.doi;

import fr.osug.doi.domain.Doi;
import fr.osug.doi.domain.DoiPublic;
import fr.osug.doi.domain.DoiStaging;
import fr.osug.doi.domain.Project;
import fr.osug.doi.domain.Status;
import fr.osug.doi.repository.DoiPublicRepository;
import fr.osug.doi.repository.DoiRepository;
import fr.osug.doi.repository.DoiStagingRepository;
import fr.osug.doi.repository.ProjectRepository;
import fr.osug.doi.service.DoiService;
import fr.osug.util.FileUtils;
import java.io.File;
import java.io.IOException;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 */
public class GenerateRedirectPipeline extends AbstractPipeline<PipelineCommonData> {

    private final static Logger logger = LoggerFactory.getLogger(GenerateRedirectPipeline.class.getName());

    public final static String FILE_HT_ACCESS = ".htaccess";

    public GenerateRedirectPipeline(final PipelineCommonData pipeData,
                                    final AbstractPipeline<?> pipe) {
        super(pipeData, pipe);
    }

    @Override
    public void doExecute() throws IOException {
        logger.info("generateRedirect ...");
        generateDOIUrls();
        generateEmbedUrls();
        logger.info("generateRedirect: done.");
    }

    private void generateDOIUrls() throws IOException {
        final StringBuilder sb = prepareBuffer();

        sb.append("\n# Redirect DOI to Landing page:");
        sb.append("\n# [/r/DOI_SUFFIX] => [landing page Ext]");
        sb.append("\n#                 or [/public/<PROJECT>/<DOI_SUFFIX>.html]     (public)");
        sb.append("\n#                 or [/staging/<PROJECT>/<DOI_SUFFIX>.html]    (staging)");

        final DoiService doiService = doiConfig.getDoiService();
        final ProjectRepository pr = doiService.getProjectRepository();
        final DoiRepository dr = doiService.getDoiRepository();
        final DoiPublicRepository dpr = doiService.getDoiPublicRepository();
        final DoiStagingRepository dsr = doiService.getDoiStagingRepository();

        final List<Project> projects = pr.findAllByOrderByNameAsc();

        // DOI listing per project:
        for (Project p : projects) {
            final String projectName = p.getName();

            sb.append("\n# Project ").append(projectName);

            final List<Doi> dois = dr.findByProject(projectName);

            logger.info("Dois for project[{}]: {}", projectName, dois.size());

            for (Doi doi : dois) {
                final String doiSuffix = doi.getIdentifier();

                if (doi.getStatus() == Status.PUBLIC) {
                    final DoiPublic dp = dpr.findOneByDoi(doi);

                    if (dp == null) {
                        logger.warn("Missing DoiPublic for [{}]; skipping Redirect", doiSuffix);
                    } else {
                        sb.append("\nRedirect \"/").append(Paths.DIR_WEB_REDIRECT).append('/');
                        sb.append(doiSuffix).append("\" \"");

                        if (dp.isActiveExtUrl() && dp.getLandingExtUrl() != null) {
                            /*
                            # [landing page Ext] case:
                            # note: complete pattern match
                            Redirect "/r/AMMA-CATCH.CE.RainD_Nc" "http://www.amma-catch.org/spip.php?article220"
                             */
                            sb.append(dp.getLandingExtUrl());
                        } else {
                            /*
                            # [/public/<PROJECT>/<DOI_SUFFIX>.html] (public) case:
                            Redirect "/r/AMMA-CATCH.all" "/public/AMMA-CATCH/AMMA-CATCH.all.html"
                             */
                            sb.append(dp.getLandingLocUrl());
                        }
                        sb.append('\"');
                    }
                } else {
                    // Status.STAGING:
                    final DoiStaging ds = dsr.findOneByDoi(doi);

                    if (ds == null) {
                        logger.warn("Missing DoiStaging for [{}]; skipping Redirect", doiSuffix);
                    } else {
                        sb.append("\nRedirect \"/").append(Paths.DIR_WEB_REDIRECT).append('/');
                        sb.append(doiSuffix).append("\" \"");

                        // Note: no test on the active flag (public only):
                        if (ds.getLandingExtUrl() != null) {
                            /*
                            # [landing page Ext] case:
                            # note: complete pattern match
                            Redirect "/r/AMMA-CATCH.CE.RainD_Nc" "http://www.amma-catch.org/spip.php?article220"
                             */
                            sb.append(ds.getLandingExtUrl());
                        } else {
                            /*
                            # [/public/<PROJECT>/<DOI_SUFFIX>.html] (public) case:
                            Redirect "/r/AMMA-CATCH.all" "/public/AMMA-CATCH/AMMA-CATCH.all.html"
                             */
                            sb.append(ds.getLandingLocUrl());
                        }
                        sb.append('\"');
                    }
                }
            }
            sb.append('\n');
        } // projects

        final String redirectRules = sb.toString();

        FileUtils.writeFile(redirectRules,
                new File(doiConfig.getPathConfig().getWebRedirectDir(), FILE_HT_ACCESS));

        logger.debug("generateDOIUrls:\n{}", redirectRules);
    }

    private void generateEmbedUrls() throws IOException {
        final StringBuilder sb = prepareBuffer();

        sb.append("\n# Redirections to embbedded page fragments:");
        sb.append("\n# note: RedirectMatch uses regexp (fuzzy match)");

        final DoiService doiService = doiConfig.getDoiService();
        final ProjectRepository pr = doiService.getProjectRepository();
        final DoiRepository dr = doiService.getDoiRepository();

        final List<Project> projects = pr.findAllByOrderByNameAsc();

        // DOI listing per project:
        for (Project p : projects) {
            final String projectName = p.getName();

            final ProjectConfig projectConfig = pipeData.getProjectConfig(projectName);

            if (projectConfig.getPropertyBoolean(ProjectConfig.CONF_KEY_GENERATE_EMBEDDED)) {
                // // [public/staging]/PROJECT/embed/DOI_SUFFIX[.html]
                sb.append("\n# Project ").append(projectName);

                /*
                RedirectMatch "^/embed/AMMA-CATCH.CE.RainD_Nc([-]*)(.*)" "/staging/AMMA-CATCH/embed/AMMA-CATCH.CE.RainD_Nc$1$2.html"
                    /embed/AMMA-CATCH.CE.RainD_Nc           =>  /staging/AMMA-CATCH/embed/AMMA-CATCH.CE.RainD_Nc.html
                    /embed/AMMA-CATCH.CE.RainD_Nc-header    =>  /staging/AMMA-CATCH/embed/AMMA-CATCH.CE.RainD_Nc-header.html
                    /embed/AMMA-CATCH.CE.RainD_Nc-meta      =>  /staging/AMMA-CATCH/embed/AMMA-CATCH.CE.RainD_Nc-meta.html
                 */
                final List<Doi> dois = dr.findByProject(projectName);

                logger.info("Dois for project[{}]: {}", projectName, dois.size());

                for (Doi doi : dois) {
                    final String doiSuffix = doi.getIdentifier();

                    // ^/embed/
                    sb.append("\nRedirectMatch \"^/").append(Paths.DIR_WEB_EMBED).append('/');
                    sb.append(doiSuffix).append("([-]*)(.*)\" \"");

                    if (doi.getStatus() == Status.PUBLIC) {
                        sb.append('/').append(Paths.DIR_WEB_PUBLIC).append('/');
                    } else {
                        sb.append('/').append(Paths.DIR_WEB_STAGING).append('/');
                    }
                    sb.append(projectName).append('/').append(Paths.DIR_WEB_EMBED).append('/').append(doiSuffix);
                    sb.append("$1$2").append(GeneratePipeline.HTML_EXT).append('\"');
                }
                sb.append('\n');
            }
        }
        sb.append('\n');

        final String redirectRules = sb.toString();

        FileUtils.writeFile(redirectRules,
                new File(doiConfig.getPathConfig().getWebRedirectEmbedDir(), FILE_HT_ACCESS));

        logger.debug("generateEmbedUrls:\n{}", redirectRules);
    }

    private StringBuilder prepareBuffer() {
        final StringBuilder sb = pipeData.buffer;
        sb.setLength(0);

        // htaccess Header:
        sb.append("# Generated at ").append(pipeData.now);
        sb.append("\n# Need AllowOverride FileInfo\n");
        return sb;
    }
}
