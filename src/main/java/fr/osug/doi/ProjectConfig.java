/*******************************************************************************
 * OSUG-DOI project ( http://doi.osug.fr ) - Copyright (C) CNRS.
 ******************************************************************************/
package fr.osug.doi;

import fr.osug.doi.domain.NameEntry;
import fr.osug.doi.validation.ValidationUtil;
import fr.osug.util.FileUtils;
import java.io.File;
import java.io.IOException;
import java.io.Reader;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 */
public final class ProjectConfig {

    private final static Logger logger = LoggerFactory.getLogger(ProjectConfig.class.getName());

    public final static String CONF_PROJECT = "project.properties";

    public final static String CONFIG_ACCESS_INSTRUCTIONS = "access_instruction.html";
    public final static String CONF_URL_MAP_DATA_ACCESS = "doi_url_data_access.csv";
    public final static String CONF_URL_MAP_LANDING_PAGE = "doi_url_landing_page.csv";

    private final static String CONF_OVERRIDE_NAMES_CSV = "names_override" + Const.FILE_EXT_CSV;

    /* project.properties */
    /** use templates for countries = "use_template_country" */
    public final static String CONF_KEY_USE_TEMPLATE_COUNTRY = "use_template_country";
    /** use templates for each DOI = "use_template_doi" */
    public final static String CONF_KEY_USE_TEMPLATE_DOI = "use_template_doi";

    /** general data access url = "dataAccessUrl" */
    public final static String CONF_KEY_DATA_ACCESS_URL = "dataAccessUrl";
    /** generate embedded fragments = "generate_embedded" */
    public final static String CONF_KEY_GENERATE_EMBEDDED = "generate_embedded";

    /* members */
    private final String projectName;
    private final File projectConf;

    // lazy:
    private Properties properties = null;
    private Map<String, String> urlMapDataAccess = null;
    private Map<String, String> urlMapLandingPages = null;
    // Directories:
    private File metadataDir = null;
    private File inputDir = null;
    private File tmpDir;
    private File tmpDoiDir;
    private File stagingDir;
    private File publicDir;
    private File webStagingProjectDir;
    private File webStagingProjectEmbedDir;
    private File webStagingProjectXmlDir;
    private File webPublicProjectDir;
    private File webPublicProjectEmbedDir;
    private File webPublicProjectXmlDir;
    // Process state:
    private DoiTemplates templates = null;
    private Map<String, NameEntry> overrideNameEntryMap = null;

    public ProjectConfig(final PathConfig pathConfig, final String projectName) throws IOException {
        this.projectName = projectName;

        // may fail:
        this.projectConf = FileUtils.getRequiredDirectory(Paths.DIR_CONFIG + projectName).getCanonicalFile();
        logger.info("Project Config: {}", projectConf);

        // preload properties to ensure the folder exists:
        getProperties();

        initMetadataDir(
                new File(this.projectConf, Paths.DIR_PROJECT_METADATA).getAbsolutePath()
        );
        initInputDir(
                new File(this.projectConf, Paths.DIR_PROJECT_INPUTS).getAbsolutePath()
        );
        initTmpDir(
                new File(Paths.DIR_TMP, projectName).getAbsolutePath()
        );
        initTmpDoiDir(
                new File(tmpDir, Paths.DIR_TMP_DOI).getAbsolutePath()
        );

        initStagingDir(
                new File(Paths.DIR_STAGING, projectName).getAbsolutePath()
        );
        initPublicDir(
                new File(Paths.DIR_PUBLIC, projectName).getAbsolutePath()
        );

        // /www/staging directories:
        initWebStagingProjectDir(
                new File(pathConfig.getWebStagingDir(), projectName).getAbsolutePath()
        );
        initWebStagingProjectEmbedDir(
                new File(webStagingProjectDir, Paths.DIR_WEB_EMBED).getAbsolutePath()
        );
        initWebStagingProjectXmlDir(
                new File(webStagingProjectDir, Paths.DIR_WEB_XML).getAbsolutePath()
        );
        // /www/public directories:
        initWebPublicProjectDir(
                new File(pathConfig.getWebPublicDir(), projectName).getAbsolutePath()
        );
        initWebPublicProjectEmbedDir(
                new File(webPublicProjectDir, Paths.DIR_WEB_EMBED).getAbsolutePath()
        );
        initWebPublicProjectXmlDir(
                new File(webPublicProjectDir, Paths.DIR_WEB_XML).getAbsolutePath()
        );
    }

    public void prepareProcess() throws IOException {
        // preload needed data for the process pipeline:
        getTemplates();
        getUrlMapDataAccess();
        getUrlMapLandingPage();
        getOverrideNameMap();
    }

    public void initMetadataDir(final String dirPath) throws IOException {
        this.metadataDir = FileUtils.getRequiredDirectory(dirPath).getCanonicalFile();
    }

    public void initInputDir(final String dirPath) throws IOException {
        this.inputDir = FileUtils.getRequiredDirectory(dirPath).getCanonicalFile();
    }

    public void initTmpDir(final String path) throws IOException {
        this.tmpDir = FileUtils.createDirectories(path);
    }

    public void initTmpDoiDir(final String path) throws IOException {
        this.tmpDoiDir = FileUtils.createDirectories(path);
    }

    public void initStagingDir(final String path) throws IOException {
        this.stagingDir = FileUtils.createDirectories(path);
    }

    public void initPublicDir(final String path) throws IOException {
        this.publicDir = FileUtils.createDirectories(path);
    }

    public void initWebStagingProjectDir(final String path) throws IOException {
        this.webStagingProjectDir = FileUtils.createDirectories(path);
    }

    public void initWebStagingProjectEmbedDir(final String path) throws IOException {
        this.webStagingProjectEmbedDir = FileUtils.createDirectories(path);
    }

    public void initWebStagingProjectXmlDir(final String path) throws IOException {
        this.webStagingProjectXmlDir = FileUtils.createDirectories(path);
    }

    public void initWebPublicProjectDir(final String path) throws IOException {
        this.webPublicProjectDir = FileUtils.createDirectories(path);
    }

    public void initWebPublicProjectEmbedDir(final String path) throws IOException {
        this.webPublicProjectEmbedDir = FileUtils.createDirectories(path);
    }

    public void initWebPublicProjectXmlDir(final String path) throws IOException {
        this.webPublicProjectXmlDir = FileUtils.createDirectories(path);
    }

    public String getProjectName() {
        return projectName;
    }

    public File getProjectConf() {
        return projectConf;
    }

    /* properties */
    private Properties getProperties() {
        if (properties == null) {
            properties = loadProperties(new File(projectConf, CONF_PROJECT));
        }
        return properties;
    }

    public String getProperty(final String key) {
        String val = getProperties().getProperty(key);
        if (val != null) {
            val = val.trim();
            if (val.length() != 0) {
                return val;
            }
        }
        return null;
    }

    public boolean getPropertyBoolean(final String key) {
        return getPropertyBoolean(key, false);
    }

    public boolean getPropertyBoolean(final String key, final boolean def) {
        final String val = getProperties().getProperty(key);
        if (val != null) {
            return Boolean.valueOf(val);
        }
        return def;
    }

    /* DataAccess Url Mapping */
    private Map<String, String> getUrlMapDataAccess() throws IOException {
        if (urlMapDataAccess == null) {
            urlMapDataAccess = CsvUtil.loadUrlMapping(new File(this.projectConf, CONF_URL_MAP_DATA_ACCESS));
        }
        return urlMapDataAccess;
    }

    public String getUrlDataAccess(final String doiSuffix) throws IOException {
        return getUrlMapDataAccess().get(doiSuffix);
    }

    /* LandingPage Url Mapping */
    private Map<String, String> getUrlMapLandingPage() throws IOException {
        if (urlMapLandingPages == null) {
            urlMapLandingPages = CsvUtil.loadUrlMapping(new File(this.projectConf, CONF_URL_MAP_LANDING_PAGE));
        }
        return urlMapLandingPages;
    }

    public String getUrlLandingPage(final String doiSuffix) throws IOException {
        return getUrlMapLandingPage().get(doiSuffix);
    }

    /* templates */
    public DoiTemplates getTemplates() throws IOException {
        if (templates == null) {
            templates = new DoiTemplates(
                    new File(this.projectConf, Paths.DIR_PROJECT_TEMPLATES).getAbsolutePath()
            );
        }
        return templates;
    }

    /* names override */
    private Map<String, NameEntry> getOverrideNameMap() throws IOException {
        if (overrideNameEntryMap == null) {
            overrideNameEntryMap = loadOverrideNames(new File(this.projectConf, CONF_OVERRIDE_NAMES_CSV));
        }
        return overrideNameEntryMap;
    }

    public NameEntry getOverrideNameEntry(final String simpleName) throws IOException {
        return getOverrideNameMap().get(simpleName);
    }

    /* Directories */
    public File getMetadataDir() {
        return metadataDir;
    }

    public File getInputDir() {
        return inputDir;
    }

    public File getTmpDir() {
        return tmpDir;
    }

    public File getTmpDoiDir() {
        return tmpDoiDir;
    }

    public File getStagingDir() {
        return stagingDir;
    }

    public File getPublicDir() {
        return publicDir;
    }

    public File getWebStagingProjectDir() {
        return webStagingProjectDir;
    }

    public File getWebStagingProjectEmbedDir() {
        return webStagingProjectEmbedDir;
    }

    public File getWebStagingProjectXmlDir() {
        return webStagingProjectXmlDir;
    }

    public File getWebPublicProjectDir() {
        return webPublicProjectDir;
    }

    public File getWebPublicProjectEmbedDir() {
        return webPublicProjectEmbedDir;
    }

    public File getWebPublicProjectXmlDir() {
        return webPublicProjectXmlDir;
    }

    private static Properties loadProperties(final File propFile) {
        logger.info("Loading {}", propFile);

        final Properties props = new Properties();
        Reader r = null;
        try {
            r = FileUtils.reader(propFile);
            props.load(r);
        } catch (IOException ioe) {
            throw new RuntimeException("loadProperties failed:" + propFile.getAbsolutePath(), ioe);
        } finally {
            FileUtils.closeFile(r);
        }
        logger.info("properties: {}", props);
        return props;
    }

    private static Map<String, NameEntry> loadOverrideNames(final File csvFile) throws IOException {
        Map<String, NameEntry> nameEntryMap = Collections.emptyMap();

        if (csvFile.exists()) {
            final CsvData data = CsvUtil.read(csvFile);
            logger.debug("CSV data: {}", data);

            final List<String[]> rows = data.getRows();
            final int nbRows = rows.size();

            if (nbRows > 1) {
                // Parse header:
                String[] keys = null;
                String[] cols = rows.get(0);

                if (cols == null || cols.length < 1) {
                    logger.error("Missing header in {}", csvFile);
                } else {
                    keys = cols;
                    // Check keys:
                    for (int i = 0; i < keys.length; i++) {
                        String key = keys[i];

                        if (key.length() > 1) {
                            if (key.charAt(0) == Const.COMMENT) {
                                key = key.substring(1);
                            }
                            key = key.trim();
                        }
                        if (key.isEmpty()) {
                            logger.error("Missing column name at {}", i);
                            key = null; // ignore
                        }
                        keys[i] = key;
                    }

                    if (logger.isDebugEnabled()) {
                        logger.debug("keys: {}", Arrays.toString(keys));
                    }

                    nameEntryMap = new LinkedHashMap<String, NameEntry>(nbRows - 1);

                    // Get mappings [Name|...]
                    for (int i = 1; i < nbRows; i++) {
                        cols = rows.get(i);

                        if (cols == null || cols.length < 1) {
                            continue;
                        }
                        String name = cols[0].trim();
                        if (name.isEmpty() || name.charAt(0) == Const.COMMENT) {
                            continue;
                        }

                        Map<String, String> attributes = null;

                        if (cols.length > 1) {
                            attributes = new HashMap<String, String>(4);

                            for (int j = 1; j < cols.length; j++) {
                                String key = keys[j];
                                String val = cols[j];
                                if (key != null && !val.trim().isEmpty()) {
                                    attributes.put(key, val);
                                }
                            }
                        }

                        final String simpleName = ValidationUtil.simplifyName(name);

                        nameEntryMap.put(simpleName, new NameEntry(name, attributes));
                    }
                }
            }
        }
        logger.debug("nameEntryMap: {}", nameEntryMap);
        return nameEntryMap;
    }
}
