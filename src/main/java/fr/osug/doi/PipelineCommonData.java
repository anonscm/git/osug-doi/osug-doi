/*******************************************************************************
 * OSUG-DOI project ( http://doi.osug.fr ) - Copyright (C) CNRS.
 ******************************************************************************/
package fr.osug.doi;

import java.io.IOException;
import java.util.Collection;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 *
 */
public class PipelineCommonData {

    public static final int BUFFER_SIZE = 4 * 1024;

    /* members */
    /** path config */
    private final PathConfig pathConfig;

    final Date now = new Date();
    /** temporary StringBuilder to hold xml document */
    final StringBuilder buffer = new StringBuilder(BUFFER_SIZE);
    /** project config instances keyed by project name */
    private final Map<String, ProjectConfig> projectConfigs = new LinkedHashMap<String, ProjectConfig>();
    /** optional doi pattern */
    private String doiPattern = null;

    public PipelineCommonData(final PathConfig pathConfig) {
        this.pathConfig = pathConfig;
    }

    public void addProjectConfig(final ProjectConfig projectConfig) {
        projectConfigs.put(projectConfig.getProjectName(), projectConfig);
    }

    public Collection<ProjectConfig> getProjectConfigs() {
        return projectConfigs.values();
    }

    public ProjectConfig getProjectConfig(final String projectName) throws IOException {
        ProjectConfig projectConfig = projectConfigs.get(projectName);
        if (projectConfig == null) {
            projectConfig = new ProjectConfig(pathConfig, projectName);
            addProjectConfig(projectConfig);
        }
        return projectConfig;
    }

    public String getDoiPattern() {
        return doiPattern;
    }

    public void setDoiPattern(String doiPattern) {
        this.doiPattern = doiPattern;
    }

}
