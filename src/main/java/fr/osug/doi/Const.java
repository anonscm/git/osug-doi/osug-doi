/*******************************************************************************
 * OSUG-DOI project ( http://doi.osug.fr ) - Copyright (C) CNRS.
 ******************************************************************************/
package fr.osug.doi;

/**
 *
 */
public interface Const {
    
    /** datacite schema version '3.1' or '4.0' */
    public final static String SCHEMA_VERSION = "3.1";
    
    public final static String DOI_PREFIX_TEST = "10.5072";

    public final static char SEPARATOR = ';';
    public final static char COMMENT = '#';
    
    public final static String FILE_EXT_CSV = ".csv";
    public final static String FILE_EXT_XML = ".xml";

    public static final String KEY_IDENTIFIER = "identifier:DOI";
    public static final String KEY_GEO_LOCATION_PLACE = "geoLocationPlace";
    public static final String KEY_TITLE = "title";

    public static final String KEY_CREATOR_NAME = "creatorName";
    public static final String KEY_CONTRIBUTOR_NAME = "contributorName";
    
    public static final String KEY_REL_ID_START = "relatedIdentifier:";
    public static final String KEY_REL_ID_DOI = ":DOI";

    public static final String[] KEY_ORDER = new String[]{
        //# 1 [identifier]
        "identifier", KEY_IDENTIFIER,
        //# 2 [creators]
        KEY_CREATOR_NAME,
        //# 3 [titles]
        KEY_TITLE,
        "title:AlternativeTitle",
        "title:Subtitle",
        "title:TranslatedTitle",
        //# 4 [publisher]
        "publisher",
        // # 5 [publicationYear]
        "publicationYear",
        //# 6 [subjects]
        "subject",
        "subject:main",
        "subject:var",
        //# 7 [contributors]
        "contributorName:ContactPerson",
        "contributorName:DataCollector",
        "contributorName:DataCurator",
        "contributorName:DataManager",
        "contributorName:Distributor",
        "contributorName:Editor",
        "contributorName:Funder",
        "contributorName:HostingInstitution",
        "contributorName:Other",
        "contributorName:Producer",
        "contributorName:ProjectLeader",
        "contributorName:ProjectManager",
        "contributorName:ProjectMember",
        "contributorName:RegistrationAgency",
        "contributorName:RegistrationAuthority",
        "contributorName:RelatedPerson",
        "contributorName:ResearchGroup",
        "contributorName:RightsHolder",
        "contributorName:Researcher",
        "contributorName:Sponsor",
        "contributorName:Supervisor",
        "contributorName:WorkPackageLeader",
        //# 8 [dates]
        "date:Accepted",
        "date:Available",
        "date:Collected",
        "date:Copyrighted",
        "date:Created",
        "date:Issued",
        "date:Submitted",
        "date:Updated",
        "date:Valid",
        //# 9 [language]
        "language",
        //# 10 [resourceType] (PARTIAL)
        "resourceType:Dataset",
        "resourceType:Service",
        "resourceType:Software",
        "resourceType:Text",
        "resourceType:Other",
        //# 11 [alternateIdentifiers] (IGNORED)
        //# 12 [relatedIdentifiers]
        // URL:
        "relatedIdentifier:Cites:URL",
        "relatedIdentifier:References:URL",
        // DOI:
        "relatedIdentifier:IsCitedBy:DOI",
        "relatedIdentifier:Cites:DOI",
        "relatedIdentifier:IsSupplementTo:DOI",
        "relatedIdentifier:IsSupplementedBy:DOI",
        "relatedIdentifier:IsContinuedBy:DOI",
        "relatedIdentifier:Continues:DOI",
        "relatedIdentifier:IsNewVersionOf:DOI",
        "relatedIdentifier:IsPreviousVersionOf:DOI",
        "relatedIdentifier:IsPartOf:DOI",
        "relatedIdentifier:HasPart:DOI",
        "relatedIdentifier:IsReferencedBy:DOI",
        "relatedIdentifier:References:DOI",
        "relatedIdentifier:IsDocumentedBy:DOI",
        "relatedIdentifier:Documents:DOI",
        "relatedIdentifier:IsCompiledBy:DOI",
        "relatedIdentifier:Compiles:DOI",
        "relatedIdentifier:IsVariantFormOf:DOI",
        "relatedIdentifier:IsOriginalFormOf:DOI",
        "relatedIdentifier:IsIdenticalTo:DOI",
        "relatedIdentifier:HasMetadata:DOI",
        "relatedIdentifier:IsMetadataFor:DOI",
        "relatedIdentifier:Reviews:DOI",
        "relatedIdentifier:IsReviewedBy:DOI",
        "relatedIdentifier:IsDerivedFrom:DOI",
        "relatedIdentifier:IsSourceOf:DOI",
        //# 13 [sizes]
        "size",
        //# 14 [formats]
        "format",
        //# 15 [version]
        "version",
        //# 16 [rightsList]
        "rights",
        //# 17 [descriptions]
        "description",
        "description:Abstract",
        "description:Methods",
        "description:SeriesInformation",
        "description:TableOfContents",
        "description:Other",
        //# 18 [geoLocations]
        KEY_GEO_LOCATION_PLACE,
        "geoLocationPoint",
        "geoLocationBox"
    };
    public static final String[] KEY_ATTRS = new String[]{
        // creator / contributor attributes:
        // nameIdentifier variants:
//        "nameIdentifier:AUTHORCLAIM",
//        "nameIdentifier:ISNI",
        "nameIdentifier:ORCID",
//        "nameIdentifier:RESEARCHERID",
//        "nameIdentifier:VIAF",
//        "nameIdentifier:URL",
        // affiliation:
        "affiliation",
        // rights attribute:
        "rightsURI"
    };
    
    public static final String[] KEY_IGNORE = new String[]{
        "Préfixe enregistrement doi",
        "Prénom Nom",
        "Attention : uniquement Prénom Nom"
    };
}
