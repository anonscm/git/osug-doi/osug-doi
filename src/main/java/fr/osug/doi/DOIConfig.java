/*******************************************************************************
 * OSUG-DOI project ( http://doi.osug.fr ) - Copyright (C) CNRS.
 ******************************************************************************/
package fr.osug.doi;

import fr.osug.doi.service.DataciteClient;
import fr.osug.doi.service.DoiService;
import fr.osug.doi.service.UrlChecker;
import fr.osug.xml.XmlFactory;
import fr.osug.xml.validator.XmlValidatorFactory;
import java.io.IOException;
import java.util.Arrays;
import javax.annotation.PostConstruct;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

/**
 *
 */
@Configuration
@ConfigurationProperties(prefix = "osug.doi")
public class DOIConfig {

    private final static Logger logger = LoggerFactory.getLogger(DOIConfig.class.getName());

    @Autowired
    private ApplicationContext appCtx;

    @Autowired
    private RestTemplateBuilder restTemplateBuilder;

    @Autowired
    private XmlFactory xmlFactory;
    @Autowired
    private XmlValidatorFactory xmlValidatorFactory;

    @Autowired
    private DoiService doiService;

    @Autowired
    private DataciteConfig dataciteConfig;

    /** url checker */
    private UrlChecker urlChecker;

    /** path config */
    private PathConfig pathConfig;

    /** debug flag */
    private boolean debug = false;

    /** DOI Prefix (registered in datacite) */
    private String prefix;

    /** DOI Domain (registered in datacite) */
    private String domain;

    /** enable/disable the Datacite API client (REST) */
    private boolean dataciteClientEnabled;

    @PostConstruct
    public void initialize() {
        try {
            this.pathConfig = new PathConfig(Paths.DIR_WEB);
        } catch (IOException ioe) {
            throw new IllegalStateException("Invalid paths: ", ioe);
        }

        this.debug = Arrays.asList(appCtx.getEnvironment().getActiveProfiles()).contains("debug");

        this.urlChecker = new UrlChecker(restTemplateBuilder);

        logger.info("debug mode: {}", debug);
        logger.info("doi prefix: {}", prefix);
        logger.info("doi domain: {}", domain);
        logger.info("Enable Datacite Client: {}", isDataciteClientEnabled());
    }

    @Bean
    @Scope(BeanDefinition.SCOPE_PROTOTYPE)
    public ProcessPipeline getProcessPipeline(final boolean saveCSV,
                                              final ProjectConfig projectConfig) {
        final ProcessPipelineData pipelineData = new ProcessPipelineData(pathConfig);
        pipelineData.addProjectConfig(projectConfig);

        return new ProcessPipeline(saveCSV, pipelineData,
                getProcessUrlPipeline(projectConfig, pipelineData)
        );
    }

    @Bean
    @Scope(BeanDefinition.SCOPE_PROTOTYPE)
    public ProcessUrlPipeline getProcessUrlPipeline(final ProjectConfig projectConfig,
                                                    final PipelineCommonData pipeData) {
        final PipelineCommonData pipelineData = createPipelineData(pipeData);
        pipelineData.addProjectConfig(projectConfig);

        return new ProcessUrlPipeline(pipelineData,
                getGeneratePipeline(true, false, pipelineData) /* staging only */
        );
    }

    @Bean
    @Scope(BeanDefinition.SCOPE_PROTOTYPE)
    public PublishPipeline getPublishPipeline(final ProjectConfig projectConfig,
                                              final String doiPattern) {
        final PipelineCommonData pipelineData = new PipelineCommonData(pathConfig);
        pipelineData.addProjectConfig(projectConfig);
        pipelineData.setDoiPattern(doiPattern);

        return new PublishPipeline(pipelineData,
                getGeneratePipeline(false, true, pipelineData) /* public only */
        );
    }

    @Bean
    @Scope(BeanDefinition.SCOPE_PROTOTYPE)
    public RemovePipeline getRemovePipeline(final ProjectConfig projectConfig,
                                            final String doiPattern) {
        final PipelineCommonData pipelineData = new PipelineCommonData(pathConfig);
        pipelineData.addProjectConfig(projectConfig);
        pipelineData.setDoiPattern(doiPattern);

        return new RemovePipeline(pipelineData,
                getGeneratePipeline(true, false, pipelineData) /* staging only */
        );
    }

    @Bean
    @Scope(BeanDefinition.SCOPE_PROTOTYPE)
    public GeneratePipeline getGeneratePipeline(final boolean doStaging, final boolean doPublic,
                                                final PipelineCommonData pipeData) {
        final PipelineCommonData pipelineData = createPipelineData(pipeData);
        return new GeneratePipeline(doStaging, doPublic, pipelineData,
                getGenerateRedirectPipeline(doStaging, doPublic, pipelineData)
        );
    }

    @Bean
    @Scope(BeanDefinition.SCOPE_PROTOTYPE)
    public GenerateRedirectPipeline getGenerateRedirectPipeline(final boolean doStaging, final boolean doPublic,
                                                                final PipelineCommonData pipeData) {
        final PipelineCommonData pipelineData = createPipelineData(pipeData);
        return new GenerateRedirectPipeline(pipelineData,
                getDataciteSyncPipeline(doStaging, doPublic, pipelineData)
        );
    }

    @Bean
    @Scope(BeanDefinition.SCOPE_PROTOTYPE)
    public DataciteSyncPipeline getDataciteSyncPipeline(final boolean doStaging, final boolean doPublic,
                                                        final PipelineCommonData pipeData) {
        final PipelineCommonData pipelineData = createPipelineData(pipeData);
        return new DataciteSyncPipeline(doStaging, doPublic, pipelineData, null);
    }

    public PathConfig getPathConfig() {
        return pathConfig;
    }

    public boolean isDebug() {
        return debug;
    }

    public ApplicationContext getAppCtx() {
        return appCtx;
    }

    public XmlFactory getXmlFactory() {
        return xmlFactory;
    }

    public XmlValidatorFactory getXmlValidatorFactory() {
        return xmlValidatorFactory;
    }

    public DoiService getDoiService() {
        return doiService;
    }

    public DataciteClient getDataciteClient() {
        return dataciteConfig.getClient();
    }

    public UrlChecker getUrlChecker() {
        return urlChecker;
    }

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public boolean isDataciteClientEnabled() {
        return dataciteClientEnabled;
    }

    public void setDataciteClientEnabled(final boolean dataciteClientEnabled) {
        this.dataciteClientEnabled = dataciteClientEnabled;
    }

    private PipelineCommonData createPipelineData(final PipelineCommonData pipeData) {
        return (pipeData != null) ? pipeData : new PipelineCommonData(pathConfig);
    }
}
