/*******************************************************************************
 * OSUG-DOI project ( http://doi.osug.fr ) - Copyright (C) CNRS.
 ******************************************************************************/
package fr.osug.doi;

import java.io.IOException;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author bourgesl
 */
public abstract class AbstractPipeline<T extends PipelineCommonData> {

    @Autowired
    protected DOIConfig doiConfig;
    // temporary data
    protected final T pipeData;
    /** child pipeline */
    private final AbstractPipeline pipe;

    public AbstractPipeline(final T pipeData,
                            final AbstractPipeline pipe) {
        this.pipeData = pipeData;
        this.pipe = pipe;
    }

    public void execute() throws IOException {
        doExecute();
        if (pipe != null) {
            pipe.execute();
        }
    }

    public abstract void doExecute() throws IOException;
}
