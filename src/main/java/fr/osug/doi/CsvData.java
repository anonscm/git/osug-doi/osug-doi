/*******************************************************************************
 * OSUG-DOI project ( http://doi.osug.fr ) - Copyright (C) CNRS.
 ******************************************************************************/
package fr.osug.doi;

import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Generic CSV Data holder
 */
public class CsvData {

    protected final static Logger logger = LoggerFactory.getLogger(CsvData.class.getName());

    protected final String filePath;
    protected final List<String[]> rows;

    CsvData(final List<String[]> rows) {
        this(null, rows);
    }

    CsvData(final String filePath, final List<String[]> rows) {
        this.filePath = filePath;
        this.rows = rows;
    }

    public final String getFilePath() {
        return filePath;
    }

    public final boolean isEmpty() {
        return rows == null || rows.isEmpty();
    }

    public final List<String[]> getRows() {
        return rows;
    }

    @Override
    public final String toString() {
        if (isEmpty()) {
            return "empty";
        }
        final StringBuilder sb = new StringBuilder(1024);

        for (int i = 0, size = rows.size(); i < size; i++) {
            final String[] cols = rows.get(i);
            if (cols != null) {
                final int len = cols.length;
                if (len == 2) {
                    sb.append('[').append(cols[0]).append("]=").append(cols[1]).append('\n');
                } else {
                    sb.append('(').append(len).append(") {");
                    for (int j = 0; j < len; j++) {
                        if (j != 0) {
                            sb.append('|');
                        }
                        sb.append(cols[j]);
                    }
                    sb.append("}\n");
                }
            }
        }
        return sb.toString();
    }

    public final String getValue(final String key) {
        for (String[] cols : rows) {
            if (cols != null && cols.length >= 2 && key.equalsIgnoreCase(cols[0])) {
                // first key found, return value:
                return cols[1];
            }
        }
        return null;
    }
}
