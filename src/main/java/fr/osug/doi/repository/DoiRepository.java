/*******************************************************************************
 * OSUG-DOI project ( http://doi.osug.fr ) - Copyright (C) CNRS.
 ******************************************************************************/
package fr.osug.doi.repository;

import fr.osug.doi.domain.Doi;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 * Spring Data JPA repository for the Doi entity.
 */
@Repository
@Transactional
public interface DoiRepository extends JpaRepository<Doi, Long> {

    @Transactional(readOnly = true)
    Doi findOneByIdentifier(String identifier);

    @Transactional(readOnly = true)
    @Query("select d from Doi d join d.project p where (p.name = :projectName) order by d.identifier asc")
    List<Doi> findByProject(@Param("projectName") String projectName);

}
