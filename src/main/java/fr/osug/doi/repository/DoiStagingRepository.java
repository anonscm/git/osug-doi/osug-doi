/*******************************************************************************
 * OSUG-DOI project ( http://doi.osug.fr ) - Copyright (C) CNRS.
 ******************************************************************************/
package fr.osug.doi.repository;

import fr.osug.doi.domain.DoiStaging;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 * Spring Data JPA repository for the DoiStaging entity.
 */
@Repository
@Transactional
public interface DoiStagingRepository extends DoiBaseRepository<DoiStaging>, JpaRepository<DoiStaging, Long> {

    @Transactional(readOnly = true)
    @Query("select ds from DoiStaging ds join ds.doi d join d.project p where (p.name = :projectName) and (ds.valid = false or ds.urlValid = false) order by d.identifier asc")
    List<DoiStaging> findErrorByProject(@Param("projectName") String projectName);

}
