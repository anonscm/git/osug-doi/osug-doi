/*******************************************************************************
 * OSUG-DOI project ( http://doi.osug.fr ) - Copyright (C) CNRS.
 ******************************************************************************/
package fr.osug.doi;

import fr.osug.util.FileUtils;
import java.io.File;
import java.io.IOException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 */
public final class Paths {

    private final static Logger logger = LoggerFactory.getLogger(Paths.class.getName());

    /** base directory of the OSUG-DOI project */
    public final static String DIR_BASE = getBasePath();

    /* [base]/conf directory */
    public final static String DIR_CONFIG = Paths.DIR_BASE + "conf/";
    /* [base]/conf/[PROJECT]/metadata/ */
    public final static String DIR_PROJECT_METADATA = "metadata/";
    /* [base]/conf/[PROJECT]/inputs/ */
    public final static String DIR_PROJECT_INPUTS = "inputs/";
    /* [base]/conf/[PROJECT]/templates/ */
    public final static String DIR_PROJECT_TEMPLATES = "templates/";

    /* [base]/resources directory */
    public final static String DIR_RESOURCES = Paths.DIR_BASE + "resources/";
    /* [base]/resources/xsd directory */
    public final static String DIR_XSD = DIR_RESOURCES + "xsd/";
    /* [base]/resources/xsd/<version>/ directory */
    public final static String DIR_XSD_SCHEMA = DIR_XSD + Const.SCHEMA_VERSION + '/';
    /* [base]/resources/xsl directory */
    public final static String DIR_XSL_ROOT = DIR_RESOURCES + "xsl/";
    /* [base]/resources/xsl directory */
    public final static String DIR_XSL = DIR_XSL_ROOT + Const.SCHEMA_VERSION + '/';

    /* [base]/tmp directory (temporary) */
    public final static String DIR_TMP = Paths.DIR_BASE + "tmp/";
    /* [base]/tmp/[PROJECT]/doi/ */
    public final static String DIR_TMP_DOI = "doi/";

    /* [base]/data directory (DOI) */
    public final static String DIR_DATA = Paths.DIR_BASE + "data/";
    /* [base]/data/staging directory (DOI) */
    public final static String DIR_STAGING = DIR_DATA + "staging/";
    /* [base]/data/public directory (DOI) */
    public final static String DIR_PUBLIC = DIR_DATA + "public/";

    /* [base]/www directory = web root */
    public final static String DIR_WEB = Paths.DIR_BASE + "www/";
    /* web sub-directory /r */
    public final static String DIR_WEB_REDIRECT = "r";
    /* web sub-directory /staging */
    public final static String DIR_WEB_STAGING = "staging";
    /* web sub-directory /public */
    public final static String DIR_WEB_PUBLIC = "public";
    /* web sub-directory /embed */
    public final static String DIR_WEB_EMBED = "embed";
    /* web sub-directory /xml */
    public final static String DIR_WEB_XML = "xml";

    private Paths() {
        // forbidden
    }

    /**
     * Return the OSUG-DOI folder path
     * @return OSUG-DOI folder path
     * @throws IllegalStateException if the base directory is invalid
     */
    private static String getBasePath() {
        try {
            String baseDir = new File(".").getCanonicalPath() + '/';
            logger.info("Runtime directory: " + baseDir);

            File dir = FileUtils.getDirectory(baseDir + "conf");

            if (dir == null) {
                // retry parent directory:
                baseDir += "../";
                dir = FileUtils.getDirectory(baseDir + "conf");

                if (dir == null) {
                    throw new IllegalStateException("Invalid Base path = " + baseDir);
                }
            }

            baseDir = dir.getParentFile().getCanonicalPath() + '/';

            return baseDir;

        } catch (IOException ioe) {
            throw new IllegalStateException("unable to get Base path: ", ioe);
        }
    }
}
