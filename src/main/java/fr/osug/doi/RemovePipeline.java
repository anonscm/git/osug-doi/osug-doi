/*******************************************************************************
 * OSUG-DOI project ( http://doi.osug.fr ) - Copyright (C) CNRS.
 ******************************************************************************/
package fr.osug.doi;

import static fr.osug.doi.GeneratePipeline.HTML_EXT;
import fr.osug.doi.domain.Doi;
import fr.osug.doi.domain.DoiStaging;
import fr.osug.doi.domain.Status;
import fr.osug.doi.repository.DoiStagingRepository;
import fr.osug.doi.service.DoiService;
import fr.osug.util.FileUtils;
import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 */
public class RemovePipeline extends AbstractPipeline<PipelineCommonData> {

    private final static Logger logger = LoggerFactory.getLogger(RemovePipeline.class.getName());

    // member:
    public RemovePipeline(final PipelineCommonData pipeData,
                          final AbstractPipeline<?> pipe) {
        super(pipeData, pipe);
    }

    @Override
    public void doExecute() throws IOException {
        for (ProjectConfig projectConfig : pipeData.getProjectConfigs()) {
            remove(projectConfig, pipeData.getDoiPattern());
        }
    }

    private void remove(final ProjectConfig projectConfig, final String doiPattern) throws IOException {
        logger.info("remove ...");

        final String pattern = convertPattern(doiPattern);
        logger.debug("pattern: {}", pattern);

        final String projectName = projectConfig.getProjectName();
        final Date now = pipeData.now;

        final DoiService doiService = doiConfig.getDoiService();
        final DoiStagingRepository dsr = doiService.getDoiStagingRepository();

        final List<DoiStaging> dsList = dsr.findByProjectAndPattern(projectName, pattern);

        logger.debug("DoiCommon list for project[{}]: {}", projectName, dsList);

        boolean changed = false;

        for (DoiStaging ds : dsList) {
            logger.debug("DoiStaging: {}", ds);

            final Doi doi = ds.getDoi();
            final String doiSuffix = doi.getIdentifier();

            if (doi.getStatus() == Status.PUBLIC) {
                logger.warn("Ignoring {} - DOI is public", doiSuffix);
            } else {
                changed |= remove(projectConfig, ds);
            }
        }
        if (changed) {
            // Update Project:
            doiService.updateProjectDate(projectName, now);
        }
    }

    private boolean remove(final ProjectConfig projectConfig, final DoiStaging ds) throws IOException {
        boolean changed = false;

        final DoiService doiService = doiConfig.getDoiService();

        final String doiSuffix = ds.getDoi().getIdentifier();
        logger.info("removing {}", doiSuffix);

        final File stagingDoiFile = FilePathUtils.getDoiMetaDataFile(projectConfig, true, doiSuffix); // staging

        if (!stagingDoiFile.exists()) {
            logger.warn("Ignoring {} - missing xml at {}", doiSuffix, stagingDoiFile);
        } else {
            changed = true;

            deleteFile(stagingDoiFile);

            // remove landing pages in staging:
            final File webDir = projectConfig.getWebStagingProjectDir();
            final File webEmbedDir = projectConfig.getWebStagingProjectEmbedDir();
            final File webXmlDir = projectConfig.getWebStagingProjectXmlDir();

            removeLandingPages(projectConfig, stagingDoiFile, webDir, webEmbedDir, webXmlDir);

            doiService.removeStagingDoiDatacite(doiSuffix);
        }
        return changed;
    }

    private void removeLandingPages(final ProjectConfig projectConfig,
                                    final File xmlFileDOI,
                                    final File webDir,
                                    final File webEmbedDir,
                                    final File webXmlDir) throws IOException {

        // Note: must be in synch with GeneratePipeline.generateLandingPages()
        final String filenameNoExt = FileUtils.getFileNameWithoutExtension(xmlFileDOI);

        // main landing page:
        // [public/staging]/PROJECT/DOI_SUFFIX.html
        File outputFile = new File(webDir, filenameNoExt + HTML_EXT);
        deleteFile(outputFile);

        if (projectConfig.getPropertyBoolean(ProjectConfig.CONF_KEY_GENERATE_EMBEDDED)) {
            // embedded mode - full:
            // [public/staging]/PROJECT/embed/DOI_SUFFIX[.html]
            outputFile = new File(webEmbedDir, filenameNoExt + HTML_EXT);
            deleteFile(outputFile);

            // embedded mode - header:
            outputFile = new File(webEmbedDir, filenameNoExt + "-header" + HTML_EXT);
            deleteFile(outputFile);

            // embedded mode - meta:
            outputFile = new File(webEmbedDir, filenameNoExt + "-meta" + HTML_EXT);
            deleteFile(outputFile);
        }

        // Xml DOI:
        outputFile = new File(webXmlDir, xmlFileDOI.getName());
        deleteFile(outputFile);
    }

    private static void deleteFile(final File file) {
        if (file.exists()) {
            logger.info("deleting {}", file);
            file.delete();
        }
    }

// TODO: refactor
    private static String convertPattern(final String doiPattern) {
        String pattern;
        // escape '%' and '_'
        pattern = doiPattern.replaceAll("_", "\\_");
        pattern = pattern.replaceAll("%", "\\%");

        // '<pattern>%' 
        return pattern + "%";
    }
}
