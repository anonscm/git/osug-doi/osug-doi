/*******************************************************************************
 * OSUG-DOI project ( http://doi.osug.fr ) - Copyright (C) CNRS.
 ******************************************************************************/
package fr.osug.doi;

import fr.osug.util.FileUtils;
import java.io.File;
import java.io.IOException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 */
public final class PathConfig {

    private final static Logger logger = LoggerFactory.getLogger(PathConfig.class.getName());

    /* members */
    private String webRootPath;
    private File webRootDir;
    private File webStagingDir;
    private File webPublicDir;
    private File webRedirectDir;
    private File webRedirectEmbedDir;

    public PathConfig(final String webRootDir) throws IOException {
        initWebRoot(webRootDir);
    }

    public void initWebRoot(final String webRootPath) throws IOException {
        logger.info("web root: {}", webRootPath);
        
        this.webRootPath = webRootPath;
        // /www/ directories:
        initWebRootDir(webRootPath);
        // /www/staging directory:
        initWebStagingDir(webRootPath + Paths.DIR_WEB_STAGING);
        // /www/public directory:
        initWebPublicDir(webRootPath + Paths.DIR_WEB_PUBLIC);
        // Redirect directories:
        // /www/r directory:
        initWebRedirectDir(webRootPath + Paths.DIR_WEB_REDIRECT);
        // /www/embed directory:
        initWebRedirectEmbedDir(webRootPath + Paths.DIR_WEB_EMBED);
    }

    private void initWebRootDir(final String path) throws IOException {
        this.webRootDir = FileUtils.createDirectories(path);
    }

    private void initWebStagingDir(final String path) throws IOException {
        this.webStagingDir = FileUtils.createDirectories(path);
    }

    private void initWebPublicDir(final String path) throws IOException {
        this.webPublicDir = FileUtils.createDirectories(path);
    }

    private void initWebRedirectDir(final String path) throws IOException {
        this.webRedirectDir = FileUtils.createDirectories(path);
    }

    private void initWebRedirectEmbedDir(final String path) throws IOException {
        this.webRedirectEmbedDir = FileUtils.createDirectories(path);
    }

    public String getWebRootPath() {
        return webRootPath;
    }

    public File getWebRootDir() {
        return webRootDir;
    }

    public File getWebStagingDir() {
        return webStagingDir;
    }

    public File getWebPublicDir() {
        return webPublicDir;
    }

    public File getWebRedirectDir() {
        return webRedirectDir;
    }

    public File getWebRedirectEmbedDir() {
        return webRedirectEmbedDir;
    }

}
