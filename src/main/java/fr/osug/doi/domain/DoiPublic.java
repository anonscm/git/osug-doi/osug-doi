/*******************************************************************************
 * OSUG-DOI project ( http://doi.osug.fr ) - Copyright (C) CNRS.
 ******************************************************************************/
package fr.osug.doi.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * A Doi in the public phase
 */
@Entity
@Table(name = "doi_public")
public class DoiPublic extends DoiCommon {

    private static final long serialVersionUID = 1L;

    @Column(name = "active_ext_url")
    private boolean activeExtUrl;

    public boolean isActiveExtUrl() {
        return activeExtUrl;
    }

    public void setActiveExtUrl(boolean activeExtUrl) {
        this.activeExtUrl = activeExtUrl;
    }

    @Override
    public String toString() {
        return "DoiPublic{"
                + super.toString()
                + ", activeExtUrl='" + activeExtUrl + "'"
                + '}';
    }
}
