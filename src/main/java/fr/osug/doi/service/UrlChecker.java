/*******************************************************************************
 * OSUG-DOI project ( http://doi.osug.fr ) - Copyright (C) CNRS.
 ******************************************************************************/
package fr.osug.doi.service;

import fr.osug.doi.DataciteConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.web.client.RestClientException;

/**
 *
 */
@Service
public class UrlChecker {

    private final static Logger logger = LoggerFactory.getLogger(DataciteConfig.class.getName());

    public final static int TIMEOUT_CONNECT = 10000;
    public final static int TIMEOUT_READ = 30000;

    private final RestTemplate restTemplate;

    public UrlChecker(RestTemplateBuilder restTemplateBuilder) {
        this.restTemplate = restTemplateBuilder
                .setConnectTimeout(TIMEOUT_CONNECT)
                .setReadTimeout(TIMEOUT_READ)
                .build();
    }

    /**
        GET resource

        Response statuses
        200 OK - operation successful
    
        @param url resource to load
        @return 
     */
    public String getResource(final String url) {
        String response = null;
        try {
            response = this.restTemplate.getForObject(url, String.class);
        } catch (RestClientException rce) {
            logger.error("getResource: failed", rce);
        }
        logger.debug("getResource:\n{}", response);
        return response;
    }

}
