/*******************************************************************************
 * OSUG-DOI project ( http://doi.osug.fr ) - Copyright (C) CNRS.
 ******************************************************************************/
package fr.osug.doi;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 */
public final class DoiTemplates {

    private static final Logger logger = LoggerFactory.getLogger(DoiTemplates.class.getName());

    /* members */
    private DoiCsvData base = null;
    private final Map<String, DoiCsvData> geoLocationPlaces = new HashMap<String, DoiCsvData>();
    private final Map<String, DoiCsvData> identifiers = new HashMap<String, DoiCsvData>();

    public DoiTemplates(final String templatePath) throws IOException {
        initialize(templatePath);
    }

    private void initialize(final String templatePath) throws IOException {
        logger.info("Loading templates from: {}", templatePath);

        final File[] csvFiles = CsvUtil.findCSVFiles(templatePath);

        for (File file : csvFiles) {
            final DoiCsvData data = new DoiCsvData(CsvUtil.read(file));
            data.trim();

            if (!data.isEmpty()) {
                if (logger.isDebugEnabled()) {
                    logger.debug("initialize: [{}] \n{}", data.getFilePath(), data);
                }

                final String doiId = data.getDoiId();
                if (doiId != null) {
                    final DoiCsvData prevData = identifiers.put(doiId, data);
                    if (prevData != null) {
                        logger.error("initialize: several files with the same identifier = '"
                                + doiId + "' = multiple doi templates:\n"
                                + prevData.getFilePath() + "\n" + data.getFilePath());
                    }
                    continue;
                }

                final String geoLocationPlace = data.getFirstGeoLocationPlace();
                if (geoLocationPlace != null) {
                    final DoiCsvData prevData = geoLocationPlaces.put(geoLocationPlace, data);
                    if (prevData != null) {
                        logger.error("initialize: several files with the same '"
                                + Const.KEY_GEO_LOCATION_PLACE + "' = '" + geoLocationPlace + "' = multiple country templates:\n"
                                + prevData.getFilePath() + "\n" + data.getFilePath());
                    }
                    continue;
                }

                // Anything left:
                if (base == null) {
                    base = data;
                } else {
                    logger.error("initialize: several files without '" + Const.KEY_IDENTIFIER
                            + "' or '" + Const.KEY_GEO_LOCATION_PLACE + "' = multiple base templates:\n"
                            + base.getFilePath() + "\n" + data.getFilePath());
                }
            }
        }

        if (logger.isDebugEnabled()) {
            logger.debug("base: \n{}", base);
            logger.debug("identifiers: \n{}", identifiers);
            logger.debug("geoLocationPlaces: \n{}", geoLocationPlaces);
        }
    }

    public DoiCsvData getBase() {
        return base;
    }

    public DoiCsvData getByIdentifier(final String id) {
        return identifiers.get(id);
    }

    public DoiCsvData getByGeoLocationPlace(final String geoLocationPlace) {
        return geoLocationPlaces.get(geoLocationPlace);
    }
}
