#!/bin/bash

BASE=http://schema.datacite.org/meta/kernel-4.0

wget $BASE/metadata.xsd

grep "xs:include" metadata.xsd > includes.txt
awk -F'"' '$0=$2' includes.txt > includes_ref.txt

FILES="$(cat includes_ref.txt)"

for f in $FILES
do
	echo "Processing $f"
	DIR=$(dirname $f)
    mkdir -p $DIR
    wget -O $f $BASE/$f
done

# cleanup
rm includes*

echo "done"
